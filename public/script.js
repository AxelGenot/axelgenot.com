// ANIMATED TEXT

function setupTypewriter(t) {
    var HTML = t.innerHTML;

    t.innerHTML = "";

    var cursorPosition = 0,
        tag = "",
        writingTag = false,
        tagOpen = false,
        typeSpeed = 100,
        tempTypeSpeed = 0;

    var type = function () {

        if (writingTag === true) {
            tag += HTML[cursorPosition];
        }

        if (HTML[cursorPosition] === "<") {
            tempTypeSpeed = 0;
            if (tagOpen) {
                tagOpen = false;
                writingTag = true;
            } else {
                tag = "";
                tagOpen = true;
                writingTag = true;
                tag += HTML[cursorPosition];
            }
        }
        if (!writingTag && tagOpen) {
            tag.innerHTML += HTML[cursorPosition];
        }
        if (!writingTag && !tagOpen) {
            if (HTML[cursorPosition] === " ") {
                tempTypeSpeed = 0;
            }
            else {
                tempTypeSpeed = (Math.random() * typeSpeed) + 50;
            }
            t.innerHTML += HTML[cursorPosition];
        }
        if (writingTag === true && HTML[cursorPosition] === ">") {
            tempTypeSpeed = (Math.random() * typeSpeed) + 50;
            writingTag = false;
            if (tagOpen) {
                var newSpan = document.createElement("span");
                t.appendChild(newSpan);
                newSpan.innerHTML = tag;
                tag = newSpan.firstChild;
            }
        }

        cursorPosition += 1;
        if (cursorPosition < HTML.length - 1) {
            setTimeout(type, tempTypeSpeed);
        }

    };

    return {
        type: type
    };
}

var typewriter = document.getElementById('typewriter');

typewriter = setupTypewriter(typewriter);

typewriter.type();


//CHEVRON


const chevron = document.querySelector('.fa-chevron-down');

window.onload = () => {

    chevron.classList.add('colorchange');

    setTimeout(function () {
        btnUniverse.classList.remove('fadeinandup')
        btnUniverse.classList.add('move');
    }, 3050);

    setTimeout(function () {
        chevron.classList.remove('colorchange');
        chevron.classList.add('bounce');
    }, 20000);



}

// UNIVERSE BUTTON

const btnUniverse = document.querySelector('.btnuniverse');
const axel = document.querySelector('.Axel');
const texty = document.querySelector('.texty');



function diveDeep() {

    setTimeout(function () {
        axel.classList.add('slideLeft');
        texty.classList.add('slideRight');
    }, 100)



    const href = this.getAttribute("href");
    const offsetTop = document.querySelector(href).offsetTop;

    setTimeout(function () {
        scroll({
            top: offsetTop,
            behavior: "smooth"
        })
    }, 900)


    btnUniverse.classList.add('scaledown');


    setTimeout(function () {
        btnUniverse.classList.remove('scaledown');
    }, 2000);

    setTimeout(function () {
        axel.classList.remove('slideLeft');
        texty.classList.remove('slideRight');
    }, 2000);
}

btnUniverse.addEventListener('click', diveDeep);


//SMOOTH SCROLLING


const navLinks = [...document.querySelectorAll(".navbar-nav a")];
const axelGenotH2 = document.querySelector('.name');
const navButton = document.querySelector('nav button');
const navShow = document.querySelector('.navbar-collapse');


for (const link of navLinks) {
    link.addEventListener("click", clickHandler);
}

chevron.addEventListener('click', clickHandler);
axelGenotH2.addEventListener('click', clickHandler)

function clickHandler(e) {
    e.preventDefault();
    const href = this.getAttribute("href");
    const offsetTop = document.querySelector(href).offsetTop;
    scroll({
        top: offsetTop,
        behavior: "smooth"
    });
    if (navButton.classList.contains('collapsed') === false) {
        navButton.classList.add('collapsed');
        navShow.classList.remove('show');
    }
}



// Adding blur to navbar

const nav = document.querySelector('.navbar');


function addBlur(e) {
    const addBlurAt = (window.scrollY + window.innerHeight);
    if (addBlurAt >= 1886) {
        nav.classList.add('blur')
    }

    if (addBlurAt < 1886) {
        nav.classList.remove('blur')

    }

};

window.addEventListener('scroll', addBlur);


// Overlap Brand effect

const overlapEls = document.querySelectorAll(".overlap") || [];

overlapEls.forEach(el => {
    const chars = [...el.textContent];
    el.innerHTML = "";
    chars.forEach((char, index) => {
        const span = document.createElement("span");
        span.textContent = char;
        span.style.setProperty("--index", index)
        el.append(span)
    })
})

//ADDING Scrolling effects on #COMPETENCES

const skillHTML = document.querySelector('#Skill-HTML');
const skillCSS = document.querySelector('#Skill-CSS');
const skillBootstrap = document.querySelector('#Skill-Bootstrap');
const skillJS = document.querySelector('#Skill-JS');
const skillReact = document.querySelector('#Skill-React');
const skillAngular = document.querySelector('#Skill-Angular');
const skillNodeJS = document.querySelector('#Skill-Node-JS');
const skillExpress = document.querySelector('#Skill-Express');
const skillMongoDB = document.querySelector('#Skill-MongoDB');
const skillTypeScript = document.querySelector('#Skill-TypeScript');

function addSkillAnimation(e) {
    const addSkillAnimationAt = (window.scrollY + window.innerHeight);

    if (addSkillAnimationAt >= 2110) {
        skillHTML.classList.add('HTMLAnim');
    }

    if (addSkillAnimationAt >= 2180) {
        skillCSS.classList.add('CSSAnim');
    }

    if (addSkillAnimationAt >= 2250) {
        skillBootstrap.classList.add('BootstrapAnim');
    }
    if (addSkillAnimationAt >= 2320) {
        skillJS.classList.add('JSAnim');
    }
    if (addSkillAnimationAt >= 2390) {
        skillReact.classList.add('ReactAnim');
    }
    if (addSkillAnimationAt >= 2460) {
        skillAngular.classList.add('AngularAnim');
    }
    if (addSkillAnimationAt >= 2530) {
        skillNodeJS.classList.add('NodeJSAnim');
    }
    if (addSkillAnimationAt >= 2600) {
        skillExpress.classList.add('ExpressAnim');
    }
    if (addSkillAnimationAt >= 2670) {
        skillMongoDB.classList.add('MongoDBAnim');
    }
    if (addSkillAnimationAt >= 2730) {
        skillTypeScript.classList.add('TypeScriptAnim');
    }
};

window.addEventListener('scroll', addSkillAnimation);


//TIMELINE

const allRonds = document.querySelectorAll('.rond');
const allBoxes = document.querySelectorAll('.box');
const allDates = document.querySelectorAll('.date')

const controller = new ScrollMagic.Controller();

allBoxes.forEach(box => {
    for (i = 0; i < allRonds.length; i++) {

        if (allRonds[i].getAttribute('data-anim') === box.getAttribute('data-anim')) {

            let tween = gsap.from(box, { y: -50, opacity: 0, duration: 0.5 })

            let scene = new ScrollMagic.Scene({
                triggerElement: allRonds[i],
                reverse: true
            })
                .setTween(tween)
                // .addIndicators()
                .addTo(controller)

        }

    }
})

allDates.forEach(date => {
    for (i = 0; i < allRonds.length; i++) {

        if (allRonds[i].getAttribute('data-anim') === date.getAttribute('data-anim')) {

            let tween = gsap.from(date, { y: -50, opacity: 0, duration: 0.5 })

            let scene = new ScrollMagic.Scene({
                triggerElement: allRonds[i],
                reverse: true
            })
                .setTween(tween)
                .addTo(controller)

        }

    }
});

//CONTACT ANIMATION

const contacth1 = document.querySelector('#contact h1');
const telBox = document.querySelector('.tel-box');
const mailBox = document.querySelector('.mail-box');
const linkedinBox = document.querySelector('.linkedin-box');
const rencontreContainer = document.querySelector('.rencontre-container');

const byline = document.querySelector('#contact h2');
bylineText = byline.innerHTML;
bylineArr = bylineText.split('');
byline.innerHTML = '';

var span;
var letter;


for (i = 0; i < bylineArr.length; i++) {
    span = document.createElement("span");
    letter = document.createTextNode(bylineArr[i]);
    if (bylineArr[i] == ' ') {
        byline.appendChild(letter);
    } else {
        span.appendChild(letter);
        byline.appendChild(span);
    }
}

function addContactAnimation(e) {
    const addContactAnimationAt = (window.scrollY + window.innerHeight);
    if (addContactAnimationAt >= 7800) {
        contacth1.classList.add('SWappear-anim');
        byline.classList.remove('d-none');
        axelGenotH2.classList.add('yellowColor');
        telBox.classList.add('fadein8-2');
        mailBox.classList.add('fadein8-3');
        linkedinBox.classList.add('fadein8-4');
        rencontreContainer.classList.remove('d-none');
    } else if (addContactAnimationAt < 7800) {
        axelGenotH2.classList.remove('yellowColor');
    }
};

window.addEventListener('scroll', addContactAnimation);





